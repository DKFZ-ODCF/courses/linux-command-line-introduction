# Course: Introduction to the Linux Commandline for Beginners

This is the teaching material for the course "Introduction to the Linux Commandline for Beginners" at the DKFZ, carried out by the Omics IT and Data Management Core Facility (ODCF).

Please browse [here](src/docs/asciidoc/Course.adoc) to see the course material.

In case you don't have access to our Jupyterhub server, you can see training files in the `training-files` directory.

## Prerequisites

Please install [Graphviz](https://plantuml.com/graphviz-dot) for the [PlantUML](https://plantuml.com/de/) diagrams to work.
On Ubuntu, you can install it with `sudo apt install graphviz`.\
Afterwards, you might need to specify the location of the `dot` executable with the environment variable `GRAPHVIZ_DOT` (`export GRAPHVIZ_DOT="$(which dot)"`).

## Build PDF

Change into the cloned git repository and run

```bash
./gradlew asciidoctorPdf
```

The PDF will be in `build/docs/asciidocPdf/Course.pdf`.

## Contributors

* Agnes Hotz-Wagenblatt
* Frank Thommen
* Martin Lang
* Gregor Warsow
* Philip R. Kensche
* Christopher Previti
* Oydin Iqbal

## Licenses

Currently, the course material has no explicit license, and can only be used in our courses.

Few files, have other licenses. See SPDX files or headers and the files in `LICENSES/` directory for details.
